# magang_batch_9_syifarizkasagita

## Fungsi penggunaan git pada project
Git merupakan version control system yang digunakan para developer untuk mengembangkan perangkat lunak  bersama-bersama. Dengan menggunakan git pada project, developer dapat berkolaborasi dengan lebih mudah karena terdapat histori perubahan code. Selain itu, proses kerja judah dimudahkan karena developer dapat bekerja secara bersamaan dengan menggunakan git branch dan berbagai fitur git lainnya.
